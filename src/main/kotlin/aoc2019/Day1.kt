package aoc2019

import java.io.File
import kotlin.math.floor
import kotlin.math.roundToLong

object Day1 {
    @JvmStatic
    fun main(args: Array<String>) {
        val sum = File("src/main/resources/input_1.txt").readLines()
                .map { s -> recursivelyCalcFuel(s.toLong(), 0) }
                .sum()
        println("Sum: $sum")
    }

    fun recursivelyCalcFuel(mass: Long, total: Long): Long {
        val fuelForFuel = calcFuel(mass)
        return if (fuelForFuel > 0) recursivelyCalcFuel(fuelForFuel, total + fuelForFuel) else total
    }

    fun calcFuel(input: Long): Long {
        return floor(input.toDouble().div(3)).roundToLong() - 2
    }

}