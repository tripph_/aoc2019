package aoc2019

import kotlin.math.ceil

object Day2 {
    fun handleOpcode(program: MutableList<Int>): MutableList<Int> {
        loop@ for (iterNum in 0..ceil(program.size.toDouble() / 4).toInt()) {
            val offset = (iterNum + 1) * 4
//            program[0] = 0
            when (program[offset - 4]) {
                1 -> {

                    //addition
                    val oldValIndex = program[program[offset - 1]]
                    val oldVal = program[oldValIndex]
                    val res = program[program[offset - 3]] + program[program[offset - 2]]
                    if (oldValIndex == 0) {
                        program[0] = res
                        if (res == 19690720) {
                            println("Noun $program[program[offset-3]] and Verb $program[program[offset-2]]")

                        }
                    }
//
                }
                2 -> {
//                    program[program[offset-1]] = 0
                    //multiplication
                    val oldValIndex = program[program[offset - 1]]
                    val oldVal = program[oldValIndex]
                    val res = program[program[offset - 3]] * program[program[offset - 2]]
//                    program[program[offset-1]] = res
                    if (oldValIndex == 0) {
                        program[0] = res
                        if (res == 19690720) {
                            println("Noun $program[program[offset-3]] and Verb $program[program[offset-2]]")
                        }
                    }
                }
                99 -> {
                    //done
                    break@loop
                }
            }
            if (program[0] == 19690720) {
                println("Noun $program[program[offset-3]] and Verb $program[program[offset-2]]")
            }
        }
        return program
    }
}