package aoc2019

import org.junit.jupiter.api.Assertions.*

internal class Day1Test {

    @org.junit.jupiter.api.Test
    fun recursivelyCalcFuel() {
        assertEquals(966, Day1.recursivelyCalcFuel(1969, 0))
        assertEquals(50346, Day1.recursivelyCalcFuel(100756, 0))
    }
}